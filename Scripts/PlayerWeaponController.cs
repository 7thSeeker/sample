﻿using UnityEngine;
using System.Collections;

public class PlayerWeaponController : MonoBehaviour 
{
    public WeaponWielded weapon;

    public delegate void Shoot(bool success);
    public static event Shoot OnShoot;

    void Update()
    {
        if (CustomInput.GetButton("Fire"))
        {
            bool success = weapon.Shoot(Camera.main.ViewportPointToRay(new Vector2(.5F, .5F)));

            if (OnShoot != null)
                OnShoot(success);
        }
    }
}
