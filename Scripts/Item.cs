﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Item : MonoBehaviour
{
    public ItemData data;

    public void PickUp()
    {

    }
}

[System.Serializable]
public struct ItemData
{
    public string @name;
    public string description;
    public GameObject prefab;
}