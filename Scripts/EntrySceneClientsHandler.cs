﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EntrySceneClientsHandler : MonoBehaviour 
{
    void Awake()
    {
        SceneManager.LoadScene(Application.isMobilePlatform ? 2 : 1);
    }
}
