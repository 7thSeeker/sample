﻿using UnityEngine;
using System.Collections;

public class PlayerCharacterInstance : MonoBehaviour 
{
    public static PlayerCharacterInstance singleton;
    public float enemyAlarmRadius = 5;
    public Collider col;

    void Awake()
    {
        singleton = this;
    }

    void Update()
    {
        if (FindObjectsOfType<PlayerCharacterInstance>().Length > 1)
            print("There is more than one player character instances in the same scene, please do something.");

        var castAllRay = new Ray(transform.position, transform.position);
        
        var alarmTargets = Physics.SphereCastAll(castAllRay, enemyAlarmRadius);

        for (int i = 0; i < alarmTargets.Length; i++)
        {
            var tmp = alarmTargets[i].collider.gameObject;
            if (tmp.GetComponent<AIBehaviour>())
            {
                var dir = (tmp.transform.position - transform.position).normalized;
                Ray feet = new Ray(col.bounds.min, dir);
                Ray chest = new Ray(col.bounds.center, dir);
                Ray head = new Ray(col.bounds.max, dir);

                var dist = Vector3.Distance(transform.position, tmp.transform.position) - 1;

                var o_feet = Physics.Raycast(feet, dist);
                var o_chest = Physics.Raycast(chest, dist);
                var o_head = Physics.Raycast(head, dist);

                if (o_feet && o_chest && o_head)
                {

                    return;
                }
                else
                {
                    tmp.GetComponent<AIBehaviour>().Alarm(this.gameObject, false);
                }
            }
        }
    }
}
