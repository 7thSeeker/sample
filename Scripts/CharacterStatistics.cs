﻿using UnityEngine;
using System.Collections;


[CreateAssetMenu]
public class CharacterStatistics : ScriptableObject
{
    public float speed;
    public float jumpForce;
}
