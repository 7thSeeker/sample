﻿using UnityEngine;

[RequireComponent(typeof(Light))]
public class Flashlight : MonoBehaviour 
{
    void FixedUpdate()
    {
        transform.position = Camera.main.transform.position;
        transform.rotation = Camera.main.transform.rotation;
    }
}
