﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(DestructibleEntity))]
public class DestructibleEntityEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var de = (DestructibleEntity)target;
        
        if (GUILayout.Button("Fill hitboxes with children colliders"))
        {
            de.hitBoxes = new List<Collider>();
            foreach(Transform child in de.transform)
            {
                if (child.gameObject.GetComponent<Collider>())
                {
                    de.hitBoxes.Add(child.gameObject.GetComponent<Collider>());
                }
            }
        }
    }
}
#endif

/// <summary>
/// This should be used as a root class for every new destructible entity in the game.
/// </summary>
public class DestructibleEntity : MonoBehaviour 
{
    public List<Collider> hitBoxes;
    public GameObject gettingDamageEffect;
    public GameObject deathEffect;
    public delegate void DeathEvent();
    public event DeathEvent OnDeathEvent;

    [SerializeField]
    float hitPoints, maxHitPoints;

#if UNITY_EDITOR
    [ContextMenu("Make hitboxes from child colliders")]
    public void FillHitBoxes()
    {
        hitBoxes = new List<Collider>();
        transform.ForEachChildrenRecursive((child) =>
            {
                if (child.GetComponent<Collider>())
                {
                    hitBoxes.Add(child.GetComponent<Collider>());
                }
            });
    }
#endif

    public bool isHitBox(Collider col)
    {
        return hitBoxes.Contains(col);
    }

    public bool isDead
    {
        get { return hitPoints <= 0; }
    }

    public virtual GameObject Spawn(Vector3 position, Quaternion rotation)
    {
        return (GameObject)Instantiate(this.gameObject, position, rotation);
    }

    public virtual void ApplyDamage(float dmg, Collider hitBox, Vector3 impactPoint = default(Vector3))
    {
        if (isDead || !hitBoxes.Contains(hitBox))
            return;

        if (gettingDamageEffect)
            Instantiate(gettingDamageEffect, impactPoint, Quaternion.identity);

        hitPoints -= dmg;

        if (isDead)
            OnDeathEvent();
    }

    protected virtual void OnEnable()
    {
        OnDeathEvent += OnDeath;
    }

    protected virtual void OnDisable()
    {
        OnDeathEvent -= OnDeath;
    }

    protected virtual void OnDeath()
    {
        if (deathEffect)
            Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    protected virtual void Update()
    {
        if (!isDead)
        {
            hitPoints = Mathf.Clamp(hitPoints, 0, maxHitPoints);
        }

    }
    

}
