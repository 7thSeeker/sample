﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Dungeon
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshCollider))]
    public class DungeonGenerator : MonoBehaviour
    {
        const float SEGMENT_WIDTH = 3;
        const float SEGMENT_HEIGHT = 5;
        const int SEGMENTS_COUNT = 1024;

        List<Vector3> occupied = new List<Vector3>();
        List<Mesh> meshBuffer = new List<Mesh>();

        Dictionary<Vector3, Room> map = new Dictionary<Vector3, Room>();

        public class Room
        {
            public float heightOffset;
            public RoomTag tag;
            public byte north, south, east, west, floor, ceil;
            public Vector3 coords;

            public Room()
            {
                heightOffset = 0;
                tag = RoomTag.None;
                north = 0;
                south = 0;
                east = 0;
                west = 0;
                floor = 1;
                ceil = 1;
            }
        }

        void FillWithCreatures()
        {
            var collection = Resources.LoadAll<Creature>(Path.CREATURES);

            foreach (var roomLocation in map.Keys)
            {
                if (Random.Range(0, 100) < 5)
                    collection.GetRandomElement<Creature>().Spawn(roomLocation + new Vector3(SEGMENT_WIDTH /2, 0, SEGMENT_WIDTH /2), Quaternion.identity);
            }
        }

        void Awake()
        {
            Vector3 o = Vector3.zero;
            Vector3 temp = Vector3.zero;

            Direction d = Direction.Forward;

            for (int i = 0; i < SEGMENTS_COUNT; i++)
            {
                Room room = new Room();

                int multiplier = 1;

                room.tag = i == 0 ? RoomTag.Spawn : room.tag;

                List<Direction> checkedDirections = new List<Direction>();
                d = RandomDirection;

                while (occupied.Contains(temp))
                {
                    if (checkedDirections.Count == 4)
                    {
                        checkedDirections = new List<Direction>();
                        multiplier++;
                    }

                    while (checkedDirections.Contains(d))
                    {
                        d = RandomDirection;
                    }

                    checkedDirections.Add(d);

                    temp = o + directions[d] * multiplier;
                }

                o = temp;
                occupied.Add(o);

                room.coords = o;


                map.Add(o, room);
            }


            foreach (var room in map.Values)
            {
                if (!map.ContainsKey(room.coords + Vector3.left * SEGMENT_WIDTH))
                {
                    room.west = 1;
                }

                if (!map.ContainsKey(room.coords + Vector3.right * SEGMENT_WIDTH))
                {
                    room.east = 1;
                }

                if (!map.ContainsKey(room.coords + Vector3.forward * SEGMENT_WIDTH))
                {
                    room.north = 1;
                }

                if (!map.ContainsKey(room.coords + Vector3.back * SEGMENT_WIDTH))
                {
                    room.south = 1;
                }
            }


        }
        Direction RandomDirection
        {
            get
            {
                var x = System.Enum.GetValues(typeof(Direction));
                return (Direction)x.GetValue(Random.Range(0, x.Length));
            }
        }

        IEnumerator Start()
        {
            Mesh mesh = new Mesh();
            List<Vector3> verts = new List<Vector3>();
            List<int> tris = new List<int>();
            List<Vector2> uv = new List<Vector2>();
            int t = 0;

            yield return null;



            for (int i = 0; i < SEGMENTS_COUNT; i++)
            {

                var m = map.Values.ElementAt(i);
                var o = m.coords;
                var hg = m.heightOffset;

                if (m.floor == 1)
                {
                    #region meshbuild
                    verts.AddRange(new Vector3[]
                {
                    new Vector3(0, hg, 0) + o,
                    new Vector3(0, hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(1 * SEGMENT_WIDTH, hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(1 * SEGMENT_WIDTH, hg, 0) + o,
                });

                    tris.AddRange(new int[]
                {
                    0 + t, 
                    1 + t, 
                    3 + t, 
                    1 + t, 
                    2 + t, 
                    3 + t,
                });
                    t += 4;

                    uv.AddRange(new Vector2[]
                    {
                        new Vector2(.5f, 0),
                        new Vector2(.5F, 1F),
                        new Vector2(1, 1),
                        new Vector2(1, 0),
                    });
                    #endregion
                }

                if (m.ceil == 1)
                {
                    #region meshbuild
                    verts.AddRange(new Vector3[]
                {
                    new Vector3(0, SEGMENT_HEIGHT + hg, 0) + o,
                    new Vector3(0, SEGMENT_HEIGHT + hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(1 * SEGMENT_WIDTH, SEGMENT_HEIGHT + hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(1 * SEGMENT_WIDTH, SEGMENT_HEIGHT + hg, 0) + o,
                });

                    tris.AddRange(new int[]
                {
                    3 + t, 
                    2 + t, 
                    0 + t, 
                    2 + t, 
                    1 + t, 
                    0 + t,
                });
                    t += 4;

                    uv.AddRange(new Vector2[]
                    {
                        new Vector2(.5f, 0),
                        new Vector2(.5F, 1F),
                        new Vector2(1, 1),
                        new Vector2(1, 0),
                    });
                    #endregion
                }

                if (m.north == 1)
                {
                    #region mesh build
                    verts.AddRange(new Vector3[]
                {
                    new Vector3(0, 0 + hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(0, SEGMENT_HEIGHT + hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(1 * SEGMENT_WIDTH, SEGMENT_HEIGHT + hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(1 * SEGMENT_WIDTH, 0 + hg, 1 * SEGMENT_WIDTH) + o,
                });

                    tris.AddRange(new int[]
                {
                    0 + t, 
                    1 + t, 
                    3 + t, 
                    1 + t, 
                    2 + t, 
                    3 + t,
                });
                    t += 4;

                    uv.AddRange(new Vector2[]
                    {
                        new Vector2(.0f, 0),
                        new Vector2(.0F, 1F),
                        new Vector2(.5F, 1),
                        new Vector2(.5F, 0),
                    });
                    #endregion
                }

                if (m.south == 1)
                {
                    #region mesh build
                    verts.AddRange(new Vector3[]
                {
                    new Vector3(1 * SEGMENT_WIDTH, 0 + hg, 0) + o,
                    new Vector3(1 * SEGMENT_WIDTH, SEGMENT_HEIGHT + hg, 0) + o,
                    new Vector3(0, SEGMENT_HEIGHT + hg, 0) + o,
                    new Vector3(0, 0 + hg, 0) + o,
                });

                    tris.AddRange(new int[]
                {
                    0 + t, 
                    1 + t, 
                    3 + t, 
                    1 + t, 
                    2 + t, 
                    3 + t,
                });
                    t += 4;

                    uv.AddRange(new Vector2[]
                    {
                        new Vector2(.0f, 0),
                        new Vector2(.0F, 1F),
                        new Vector2(.5F, 1),
                        new Vector2(.5F, 0),
                    });
                    #endregion
                }
                if (m.west == 1)
                {
                    #region mesh build
                    verts.AddRange(new Vector3[]
                {
                    new Vector3(0, 0 + hg, 0) + o,
                    new Vector3(0, SEGMENT_HEIGHT + hg, 0) + o,
                    new Vector3(0, SEGMENT_HEIGHT + hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(0, 0 + hg, 1 * SEGMENT_WIDTH) + o,
                });

                    tris.AddRange(new int[]
                {
                    0 + t, 
                    1 + t, 
                    3 + t, 
                    1 + t, 
                    2 + t, 
                    3 + t,
                });
                    t += 4;

                    uv.AddRange(new Vector2[]
                    {
                        new Vector2(.0f, 0),
                        new Vector2(.0F, 1F),
                        new Vector2(.5F, 1),
                        new Vector2(.5F, 0),
                    });
                    #endregion
                }
                if (m.east == 1)
                {
                    #region mesh build
                    verts.AddRange(new Vector3[]
                {
                    new Vector3(1 * SEGMENT_WIDTH, 0 + hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(1 * SEGMENT_WIDTH, SEGMENT_HEIGHT + hg, 1 * SEGMENT_WIDTH) + o,
                    new Vector3(1 * SEGMENT_WIDTH, SEGMENT_HEIGHT + hg, 0) + o,
                    new Vector3(1 * SEGMENT_WIDTH, 0 + hg, 0) + o,
                });

                    tris.AddRange(new int[]
                {
                    0 + t, 
                    1 + t, 
                    3 + t, 
                    1 + t, 
                    2 + t, 
                    3 + t,
                });
                    t += 4;

                    uv.AddRange(new Vector2[]
                    {
                        new Vector2(.0f, 0),
                        new Vector2(.0F, 1F),
                        new Vector2(.5F, 1),
                        new Vector2(.5F, 0),
                    });
                    #endregion
                }
            }

            mesh.vertices = verts.ToArray();
            mesh.triangles = tris.ToArray();
            mesh.uv = uv.ToArray();
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            mesh.Optimize();


            GetComponent<MeshFilter>().sharedMesh = mesh;
            GetComponent<MeshCollider>().sharedMesh = mesh;

            meshBuffer.Add(mesh);

            FillWithCreatures();

            GameObject.FindGameObjectWithTag("Player").transform.position = map.Values.First(x => x.tag == RoomTag.Spawn).coords;
        }



        public enum RoomTag
        {
            None,
            Spawn,
        }

        public enum Direction
        {
            Right, 
            Left,
            Forward,
            Back,
        }

        static Dictionary<Direction, Vector3> directions = new Dictionary<Direction, Vector3>()
        {
            { Direction.Right, new Vector3(1 * SEGMENT_WIDTH, 0, 0) },
            { Direction.Left, new Vector3(-1 * SEGMENT_WIDTH, 0, 0) },
            { Direction.Forward, new Vector3(0, 0, 1 * SEGMENT_WIDTH) },
            { Direction.Back, new Vector3(0, 0, -1 * SEGMENT_WIDTH) },
        };

    }
}