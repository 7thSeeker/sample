﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using ArgumentException = System.ArgumentException;
using Array = System.Array;
using Enum = System.Enum;
using Math = System.Math;
using Random = UnityEngine.Random;
using String = System.String;

public static class Extensions
{
    public static GameObject CreateDummy(this GameObject go, string name = "empty gameobject", Transform parent = null, bool isStatic = false)
    {
        go.transform.parent = parent;
        go.name = name;
        go.isStatic = isStatic;
        return go;
    }

    public static void ForEachChildrenRecursive(this Transform parent, System.Action<Transform> action)
    {
        foreach (Transform child in parent)
        {
            action(child);
            ForEachChildrenRecursive(child, action);
        }
    }

    public static Transform FindChildRecursive(this Transform aParent, string aName)
    {
        var result = aParent.Find(aName);
        if (result != null)
            return result;
        foreach (Transform child in aParent)
        {
            result = child.FindChildRecursive(aName);
            if (result != null)
                return result;
        }
        return null;
    }

    public static T GetRandomElement<T>(this object[] array)
    {
        return (T)array[Random.Range(0, array.Length)];
    }

    public static T GetRandomElement<T>(this List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }

    public static Vector3 RoundVector(Vector3 vector, int digits)
    {
        vector.x = (float)Math.Round(vector.x, digits);
        vector.y = (float)Math.Round(vector.y, digits);
        vector.z = (float)Math.Round(vector.z, digits);
        return vector;
    }

    public static Transform DeepTransformFind(Transform root, string _name, bool createIfNoExists = false)
    {
        var result = root.Find(_name);
        if (result != null)
            return result;

        foreach (Transform t in root)
        {
            result = DeepTransformFind(t, _name);
            if (result != null)
                return result;
        }
        return createIfNoExists ? new GameObject().transform : null;
    }


    public static T Next<T>(this T src) where T : struct
    {
        if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

        T[] Arr = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf<T>(Arr, src) + 1;
        return (Arr.Length == j) ? Arr[0] : Arr[j];
    }
}


    public class Binaries
    {
        public static byte[] ObjectToByteArray(System.Object obj)
        {
            if (obj == null)
                return null;

            var bf = new BinaryFormatter();
            var ms = new MemoryStream();

            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        public static System.Object ByteArrayToObject(byte[] buffer)
        {
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();

            ms.Write(buffer, 0, buffer.Length);
            ms.Seek(0, SeekOrigin.Begin);
            var obj = bf.Deserialize(ms);

            return obj;
        }
    }

