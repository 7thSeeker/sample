﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Enum = System.Enum;
using WebSocketSharp;
using WebSocketSharp.Net;
using WebSocketSharp.Server;
using UnityEngine.SceneManagement;

public class AndroidControllerClient : MonoBehaviour 
{
    WebSocket client;
    public string url = "ws://192.168.1.36:8080/control";

    public Text infoField;


    void Start()
    {

        //if (!Input.location.isEnabledByUser)
           // Application.Quit();
        Input.location.Start();
        Input.compensateSensors = true;
        Input.gyro.enabled = true;
        Input.compass.enabled = true;
        client = new WebSocket(url);

        client.OnOpen += client_OnOpen;
        client.OnMessage += client_OnMessage;
        client.OnError += client_OnError;
        client.OnClose += client_OnClose;

        client.Connect();

        //Input.compass.enabled = true;
        //Input.location.Start();
    }

    public void ChangeOrientation()
    {
        Screen.orientation = Screen.orientation.Next();
    }

    void client_OnClose(object sender, CloseEventArgs e)
    {
        print("client: close");
    }

    void client_OnError(object sender, ErrorEventArgs e)
    {
        print("client: something went wrong");
    }

    void client_OnMessage(object sender, MessageEventArgs e)
    {
        print("cleint: message received");
    }

    void client_OnOpen(object sender, System.EventArgs e)
    {
        print("client: open");
    }

    
    void Update()
    {


        infoField.text =
            string.Format("Acceleration: {0}\nGyroAttitude: {1}\nGyroUserAcceleration: {2}\nGPS: {3}\nCompensateSensors: {4}",
            Input.acceleration.ToString(), 
            Input.gyro.enabled ? Input.gyro.attitude.ToString() : "N/A", 
            Input.gyro.enabled ? Input.gyro.userAcceleration.ToString() : "N/A",
            Input.location.lastData.latitude + "|" + Input.location.lastData.longitude,
            Input.compensateSensors);

            var to = Binaries.ObjectToByteArray(new MobileInputData(Input.acceleration, Input.location.lastData));
            client.Send(to);
        

        
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit()
    {
        Application.Quit();
    }

}

[System.Serializable]
public struct MobileInputData
{

    public float[] acceleration;
    public float[] location;
    public float[] precision;

    public MobileInputData(Vector3 accelerations, LocationInfo location)
    {
        acceleration = new float[3] { accelerations.x, accelerations.y, accelerations.z, };
        this.location = new float[2] { location.latitude, location.longitude, };
        precision = new float[2] { location.horizontalAccuracy, location.verticalAccuracy, };
    }


}