﻿using UnityEngine;
using System.Collections;
using WebSocketSharp;
using WebSocketSharp.Net;
using WebSocketSharp.Server;

#region Custom Editor
#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(AndroidControllerEntryPoint))]
public class AndroidControllerEntryPointEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var acep = (AndroidControllerEntryPoint)target;

        if (acep.server != null)
        {

            var info = string.Format("Listening state: {0}\nAddress: {1}:{2}", acep.server.IsListening, acep.server.Address, acep.server.Port);

            EditorGUILayout.SelectableLabel(info);

        }
    }
}
#endif
#endregion

public class AndroidControllerEntryPoint : MonoBehaviour 
{
    public WebSocketServer server { get; private set; }

    void Start()
    {
        server = new WebSocketServer("ws://192.168.1.36:8080");
        server.AddWebSocketService<ControllerService>("/control");
        server.Start();
    }
  
    void OnDisable()
    {
        server.Stop();
    }
}

public class ControllerService : WebSocketBehavior
{
    public static Vector3 acceleration { get; private set; }
    Vector3 prevLocation;

    protected override void OnError(ErrorEventArgs e)
    {
        Debug.Log("Something went wrong: " + e.Message);
    }
    protected override void OnOpen()
    {
        Debug.Log("Connection opened");
    }
    protected override void OnMessage(MessageEventArgs e)
    {
        prevLocation.ToString();
        var msg = (MobileInputData)Binaries.ByteArrayToObject(e.RawData);
        acceleration = new Vector3(msg.acceleration[0], 0, msg.acceleration[1]); //accel (gyro)
        //acceleration = new Vector3(msg.gyroAttitude[0], 0, msg.gyroAttitude[1]); //not working

        //#region gps acceleration
        //var newLocation = new Vector3(msg.location[0], 0, msg.location[1]);
        //acceleration = newLocation - prevLocation; //gps
        //prevLocation = newLocation;
        //Debug.Log(acceleration.x + "|" + acceleration.z);

        //#endregion
    }
}
