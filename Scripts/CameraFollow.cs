﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
    public Transform target;
    public Vector3 offset;
    [Range(0,1)]
    public float lerpSpeed = 1;

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target.position + offset, lerpSpeed);
    }
}
