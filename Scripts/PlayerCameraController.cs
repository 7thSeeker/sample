﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(PlayerCameraController))]
public class PlayerCameraControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var pcc = (PlayerCameraController)target;

        EditorGUILayout.LabelField("Bob: " + pcc.bobAngle);
    }
}

#endif

[RequireComponent(typeof(PlayerMovementController))]
public class PlayerCameraController : MonoBehaviour 
{
    Camera cam { get { return Camera.main; } }

    public Vector3 cameraOffset;

    float mx { get { return Input.GetAxis("Mouse X") * 10; } }
    float my { get { return Input.GetAxis("Mouse Y") * 10; } }
    PlayerMovementController pmc { get { return GetComponent<PlayerMovementController>(); } }

    [Range(1,10)]
    public float mouseSensitivityX, mouseSensitivityY;

    [Range(0, 90)]
    public float clampAngle;

    public bool headBob;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position + _cameraOffset, .1F);
    }

    public Vector3 h_rotation 
    { 
        get 
        {
            var rot = transform.rotation.eulerAngles;
            rot.y += mx * mouseSensitivityX * Time.deltaTime;
            return rot;
        } 
    }


    float xCamAxis = 0;
    public Vector3 v_rotation
    {
        get 
        {
            var rot = cam.transform.rotation.eulerAngles;

            xCamAxis += my * mouseSensitivityY * Time.deltaTime * (-1);

            xCamAxis = Mathf.Clamp(xCamAxis, -clampAngle, clampAngle);

            rot.x = xCamAxis;

            rot.y = 0;
            rot.z = 0;

            return rot;
        }
    }

    Vector3 _cameraOffset
    {
        get
        {
            var result = cameraOffset;

            if (headBob)
            {
                result.x += bobAngle;
            }


            bobAngle = Mathf.PingPong(pmc.getSpeedMagnitude * Time.deltaTime, 1);

            return transform.TransformDirection(result);
        }
    }

    public float bobAngle { get; private set; }
    void Update()
    {
        cam.transform.position = transform.position + _cameraOffset;
        cam.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + v_rotation);
        transform.rotation = Quaternion.Euler(h_rotation);
    }


}

