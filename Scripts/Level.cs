﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level : MonoBehaviour 
{
    public List<Vector3> respawnPoints;

    public static Level instance;

    void Awake()
    {
        instance = this;
    }

    void OnDrawGizmos()
    {
        respawnPoints.ForEach(x =>
            {
                Gizmos.color = Color.red;
                Gizmos.DrawCube(x, Vector3.one * 2);
            });
    }

    public class Objective
    {
        public string name { get; protected set; }
        public bool winCondition { get; protected set; }

        public Objective (string name, bool winCondition)
        {
            this.name = name;
            this.winCondition = winCondition;
        }
    }
}
