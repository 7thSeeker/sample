﻿using UnityEngine;
using System.Collections;

public class Ghoul : Creature
{
    public Animator anim;
    public AIBehaviour ai { get { return GetComponent<AIBehaviour>(); } }

    

    protected override void OnDeath()
    {
        ai.enabled = false;
    }

    protected override void Update()
    {
        base.Update();
        GetComponent<Collider>().enabled = !isDead;
        anim.SetBool("Dead", isDead);
        anim.SetFloat("Speed", ai.v);
    }
}
