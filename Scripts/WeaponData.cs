﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class WeaponData : ScriptableObject
{
    public float range, damage, cooldown, recoil;
    public bool pierce;
    public GameObject impactEffect;
    
}
