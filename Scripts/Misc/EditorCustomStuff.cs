﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class EditorCustomStuff
{
    [MenuItem("GameObject/Utils/Remove all collisions in children", false, 0)]
    public static void RemoveAllCollisions()
    {
        var selection = Selection.transforms;
        Debug.Log(selection.Length);

        for (int i = 0; i < selection.Length; i++)
        {
            selection[i].ForEachChildrenRecursive((child) =>
                {
                    if (child.GetComponent<Collider>())
                        MonoBehaviour.DestroyImmediate(child.GetComponent<Collider>());
                });
        }
    }

    static Dictionary<string, string> tagsNames = new Dictionary<string, string>()
    {
        { "Environment", "STATIC_ENVIRONMENT" },
        { "Handler", "SYSTEM_HANDLERS" },
    };

    [ExecuteInEditMode]
    [MenuItem("OnlyWatch/Sort objects on scene")]
    public static void SortObjectsOnScene()
    {
        var values = tagsNames.Values.ToArray();

        for(int i = 0; i < values.Length; i++)
        {
            if (!GameObject.Find(values[i]))
            {
                new GameObject().CreateDummy(values[i], null);
            }
        }

        var sgo = GameObject.FindObjectsOfType<GameObject>();
        for (int i = 0; i < sgo.Length; i++)
        {
            if (sgo[i].transform.root == sgo[i].transform)
            {
                string value = string.Empty;
                if (tagsNames.TryGetValue(sgo[i].tag, out value))
                {
                    sgo[i].transform.parent = GameObject.Find(value).transform;
                }
            }
        }
    }

    [ExecuteInEditMode]
    [MenuItem("OnlyWatch/Create new level")]
    public static void CreateLevel()
    {
        EditorSceneManager.MarkAllScenesDirty();
        var scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);

        var r = Resources.LoadAll("NewLevelPrefabs");
        for (int i = 0; i < r.Length; i++)
        {
            var g = (GameObject)PrefabUtility.InstantiatePrefab(r[i]);
            g.name.Replace("(Clone)", string.Empty);
        }
    }
}
#endif