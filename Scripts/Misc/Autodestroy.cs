﻿using UnityEngine;
using System.Collections;

public class Autodestroy : MonoBehaviour 
{
    public float lifetime;
    public Renderer _renderer;
    public string textureName;
    Color color;

    void Start()
    {
        Destroy(gameObject, lifetime);

        color = _renderer.sharedMaterial.GetColor(textureName);
    }

    void Update()
    {
        color.a -= Time.deltaTime / lifetime;
        _renderer.material.SetColor(textureName, color);
    }
}

