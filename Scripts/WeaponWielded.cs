﻿using UnityEngine;
using System.Collections;
using System.Linq;

#region custom editor
#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(WeaponWielded))]
public class WeaponControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}
#endif
#endregion 

public class WeaponWielded : MonoBehaviour 
{
    public Collider[] ignoreColliders;
    public WeaponData weaponData;
    float cooldown;

    public virtual bool Shoot(Ray ray)
    {
        if (cooldown <= 0)
        {
            ray.direction += (Vector3)Random.insideUnitCircle * weaponData.recoil;
            var hits = Physics.RaycastAll(ray, weaponData.range).OrderBy((x) => x.distance).ToArray();
            for (int i = 0; i < hits.Length; i++ )
            {
                if (ignoreColliders.All(x => x != hits[i].collider))
                {

                    var root = hits[i].collider.transform.root;

                    var dmge = root.GetComponent<DestructibleEntity>();

                    if (dmge && !dmge.isHitBox(hits[i].collider))
                        continue;

                    if (dmge)
                        dmge.ApplyDamage(1f, hits[i].collider, hits[i].point);

                    var ai = root.GetComponent<AIBehaviour>();
                    if (ai)
                        ai.Alarm(this.gameObject, true);

                    var impact = (GameObject)Instantiate(weaponData.impactEffect, hits[i].point, Quaternion.FromToRotation(Vector3.up, hits[i].normal));
                    impact.transform.parent = hits[i].transform;
                    if (!weaponData.pierce)
                        break;
                }



            }
            cooldown = weaponData.cooldown;
            return true;
        }

        return false;
    }

    public virtual void Update()
    {
        cooldown = Mathf.Clamp(cooldown - Time.deltaTime, 0, byte.MaxValue);
    }


}
