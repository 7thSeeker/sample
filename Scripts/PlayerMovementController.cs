﻿using UnityEngine;
using System.Collections;

#region Custom Editor
#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(PlayerMovementController))]
public class PlayerMovementControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}
#endif
#endregion

public enum ControllerType
{
    PCKeyboard,
    MobileDevice,
}

public enum ControlMethod
{
    CharacterController,
    Rigidbody,
}

public class PlayerMovementController : MonoBehaviour
{
    #region public
    public Rigidbody rigidBody;
    public CharacterController characterController;
    public CharacterStatistics characterStatistics;
    public float speedFactor = 1;
    public float jumpForceFactor = 1;
    public ControllerType controllerType;
    public ControlMethod controlMethod;
    #endregion

    float h { get { return controllerType == ControllerType.PCKeyboard ? CustomInput.GetAxis("Horizontal") : ControllerService.acceleration.x; } }
    float v { get { return controllerType == ControllerType.PCKeyboard ? CustomInput.GetAxis("Vertical") : ControllerService.acceleration.z; } }
    float speed { get { return characterStatistics.speed * speedFactor; } }
    float jumpSpeed { get { return characterStatistics.jumpForce * jumpForceFactor; } }
    public float g { get; private set; }
    bool jump { get { return CustomInput.GetButtonDown("Jump"); } }

    public Vector3 Direction(bool jump)
    {
        g = characterController.isGrounded ? 0 : g < -100 ? -100 : g;

        var d = new Vector3();

        d.x += h * speed * speedFactor * Time.deltaTime;
        d.z += v * speed * speedFactor * Time.deltaTime;
        g = jump && characterController.isGrounded ? jumpSpeed : g + Time.deltaTime * Physics.gravity.y;

        d.y += g * Time.deltaTime;

        return transform.TransformDirection(d);
    }

    public float getSpeedMagnitude
    {
        get
        {
            var r = characterController.velocity;
            r.y = 0;
            return r.magnitude;
        }
    }

    public Vector3 RigidbodyDirection()
    {
        var d = new Vector3();

        d.x += h * speed * speedFactor * Time.deltaTime;
        d.z += v * speed * speedFactor * Time.deltaTime;

        return rigidBody.position + transform.TransformDirection(d);
    }

    void FixedUpdate()
    {
        switch(controlMethod)
        {
            case ControlMethod.CharacterController:
                characterController.Move(Direction(CustomInput.GetButton("Jump")));
                break;
            case ControlMethod.Rigidbody:
                rigidBody.MovePosition(RigidbodyDirection());
                break;
        }
    }

    void Update()
    {
        if (rigidBody && jump && Mathf.Abs(rigidBody.velocity.y) <= 0.1F)
        {
            rigidBody.AddForce(transform.up * jumpSpeed, ForceMode.VelocityChange);
        }
    }


}
