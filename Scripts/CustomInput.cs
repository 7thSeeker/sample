﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#region Custom Editor
#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(CustomInput))]
public class CustomInputEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}
#endif
#endregion

public class CustomInput : MonoBehaviour
{
    #region singleton
    public static CustomInput instance;
    [ExecuteInEditMode]
    void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        instance = this;
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    [SerializeField]
    List<Axis> axes;

    public void IterateAxes(System.Action<Axis> action)
    {
        axes.ForEach(x => action(x));
    }

    public static bool GetButtonDown(string button)
    {
        bool result = false;

        for (int i = 0; i < instance.axes.Count; i++)
        {
            if (instance.axes[i].name == button)
            {
                result = instance.axes[i].active;
            }
        }

        return result == false && GetButton(button);
    }

    public static bool GetButton(string button)
    {
        string a = string.Empty;
        string b = string.Empty;

        for (int i = 0; i < instance.axes.Count; i++)
        {
            if (instance.axes[i].name == button)
            {
                a = instance.axes[i].positiveButton;
                b = instance.axes[i].negativeButton;
                break;
            }
        }
        bool keyA = false;
        bool keyB = false;

        if (a != string.Empty)
            keyA = Input.GetKey(instance.axes.First(x => x.name == button).positiveButton);

        if (b != string.Empty)
            keyB = Input.GetKey(instance.axes.First(x => x.name == button).negativeButton);

        return keyA | keyB;
    }

    public static float GetAxisRaw(string button)
    {
        var a = GetAxis(button);
        return a < 0 ? -1 : a > 0 ? 1 : 0;
    }

    public static float GetAxis(string button)
    {
        return instance.axes.First(x => x.name == button).range;
    }

    void LateUpdate()
    {
        for (int i = 0; i < axes.Count; i++)
        {
            var x = axes[i];

            bool positive = false;
            bool negative = false;

            if (!string.IsNullOrEmpty(x.positiveButton))
                positive = Input.GetKey(x.positiveButton);
            if (!string.IsNullOrEmpty(x.negativeButton))
                negative = Input.GetKey(x.negativeButton);

            x.active = positive | negative;

            bool dead = Mathf.Abs(x.range) <= x.dead;
            //x.range = dead ? 0 : x.range;

            //float v = positive ? 1 : negative ? -1 : 0;
            float ratio = Time.deltaTime * (positive || negative ? x.sensitivity : x.gravity);

            //x.range = Mathf.Lerp(x.range - (v * Time.deltaTime), v, ratio);

            float temp = x.range;

            x.range = positive ? x.range + ratio : negative ? x.range - ratio : dead ? 0 : x.range + ratio * (x.range > 0 ? -1 : 1);

            x.range = Mathf.Clamp((x.range < 0 && temp > 0) | (x.range > 0 && temp < 0) ? 0 : x.range, -1, 1);

            if (x.snap)
            x.range = positive && x.range < 0 ? 0 : negative && x.range > 0 ? 0 : x.range;

            //x.range = Mathf.SmoothStep(x.range, v, ratio);

            axes[i] = x;
        }
    }


}

[System.Serializable]
public struct Axis
{
    public string
        name,
        positiveButton,
        negativeButton;
    public float
        range,
        gravity,
        dead,
        sensitivity;
    public bool snap;

    public bool active;
}
