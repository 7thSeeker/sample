﻿using UnityEngine;
using System.Collections;

public class AIBehaviour : MonoBehaviour
{
    #region public
    public Rigidbody rigidBody;
    public CharacterController characterController;
    //public CharacterStatistics characterStatistics;

    public float speed;
    public float jumpForce;

    public float speedFactor = 1;
    public float jumpForceFactor = 1;

    public float stoppingDistance = 2;
    public float alarmOthersRadius = 5;

    public GameObject targetObject;
    public Vector3 targetLocation;
    #endregion

    public float v { get; private set; }
    float _speed { get { return speed * speedFactor; } }
    float _jumpSpeed { get { return jumpForce * jumpForceFactor; } }
    public float g { get; private set; }


    Vector3 targetLocationFixedY
    {
        get
        {
            var tmp = targetLocation;
            tmp.y = transform.position.y;
            return tmp;
        }
    }

    protected virtual void Awake()
    {
        targetLocation = transform.position;
    }

    public virtual void Alarm(GameObject target, bool alarmOthers)
    {
        this.targetLocation = target.transform.position;
        this.targetObject = target;

        if (alarmOthers)
        {
            foreach (var subject in Physics.SphereCastAll(new Ray(transform.position, transform.position), alarmOthersRadius))
            {
                var aiEntity = subject.transform.GetComponent<AIBehaviour>();

                if (aiEntity)
                    aiEntity.Alarm(target, false);
            }
        }
    }

    protected virtual void Update()
    {
        transform.LookAt(targetLocationFixedY);

        if (Vector3.Distance(transform.position, targetLocation) >= stoppingDistance)
        {

            if (targetObject != null)
            {
                targetLocation = targetObject.transform.position;
            }
            

            
            v += Time.deltaTime;
            characterController.Move(Direction(false));

        }
        else
        {
            v -= Time.deltaTime;           
        }

        v = Mathf.Clamp01(v);

        //targetObject = null;
    }

    protected Vector3 Direction(bool jump)
    {
        g = characterController.isGrounded ? 0 : g < -100 ? -100 : g;

        var d = new Vector3();
        d.z += v * speed * speedFactor * Time.deltaTime;
        g = jump && characterController.isGrounded ? _jumpSpeed : g + Time.deltaTime * Physics.gravity.y;

        d.y += g * Time.deltaTime;

        return transform.TransformDirection(d);
    }

    protected float getSpeedMagnitude
    {
        get
        {
            var r = characterController.velocity;
            r.y = 0;
            return r.magnitude;
        }
    }

    protected Vector3 RigidbodyDirection()
    {
        var d = new Vector3();

        d.z += v * speed * speedFactor * Time.deltaTime;

        return rigidBody.position + transform.TransformDirection(d);
    }

}
