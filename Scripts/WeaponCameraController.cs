﻿using UnityEngine;
using System.Collections;

public class WeaponCameraController : MonoBehaviour 
{
    public Transform machinegun;

    void OnEnable()
    {
        PlayerWeaponController.OnShoot += PlayerWeaponController_OnShoot;
    }

    void OnDisable()
    {
        PlayerWeaponController.OnShoot -= PlayerWeaponController_OnShoot;
    }

    void PlayerWeaponController_OnShoot(bool success)
    {

            var r = machinegun.rotation.eulerAngles;
            r.z += Time.deltaTime * 500;
            machinegun.rotation = Quaternion.Euler(r);
        

    }

}
