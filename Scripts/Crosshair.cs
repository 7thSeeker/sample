﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour 
{
    void Update()
    {
        var p = Camera.main.ViewportToWorldPoint(new Vector3(.5F, .5F));
        p.z += 100;
        transform.position = p;
    }
}
